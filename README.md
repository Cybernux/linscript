
![](artwork/linscript-logo-v1_150-50.png)


***LinScript*** is a set of scripts designed to build a Linux Distribution based  heavily on the [LFS and BLFS Books](https://linuxfromscratch.org). 

The scripts are written for the most part in bash. They will be made extendable, so that any missing packages can be easily added.


## Documentation
Scarce for now but will be added as time goes. They will be in the [WIKI](../../wikis/home) or on the [Cybernux website](https://cybernux.org)


## License
**MIT**
Read the [LICENSE](LICENSE) for Details.


## Misc. Info
These scripts are heavily based on the [lfs-scripts](https://github.com/emmett1/lfs-scripts) from _emmett1_ at [GitHub](https://github.com).

See the [original README](README.md.orig) for details about [lfs-scripts](https://github.com/emmett1/lfs-scripts).

